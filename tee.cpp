/*
 * UNIX tee command using threads
 * author: Vishnu Ramesh Maroli
 */

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <string>
#include <queue>

using namespace std;

// struct definition
typedef struct {
    ostream& stream;
    queue<string>& buffer;
} threadInfoStruct;

// global variables
bool finishedInputReading;
queue<string> queueForOutputBuffer;
queue<string> queueForFileBuffer;

// function defs
void* readFromStream(void *args);
void* writeToStream(void *args);

int main(int argc, char *argv[]) {
	// arguement handling
	if(argc > 1);
	else {
		cerr << "usage: tee [filename]" << endl ;
		return 1;
	}

	finishedInputReading = false;
	ofstream out (argv[1], ofstream::out);

	pthread_t threadForInput;
	pthread_create(&threadForInput, NULL, &readFromStream, (void*)&cin);

	pthread_t threadForOutput;
	threadInfoStruct *forOutput = new threadInfoStruct{cout, queueForOutputBuffer};
	pthread_create(&threadForOutput, NULL, &writeToStream, (void*)forOutput);

	pthread_t threadForFile;
	threadInfoStruct *forFile = new threadInfoStruct{out, queueForFileBuffer};
	pthread_create(&threadForFile, NULL, &writeToStream, (void*)forFile);

	pthread_join(threadForInput, NULL);
	pthread_join(threadForOutput, NULL);
	pthread_join(threadForFile, NULL);

	out.close();
    return 0;
}

void* readFromStream(void *args) {
	istream *stream = (istream*)args;
	string in;
	while(!stream->eof()) {
		getline(*stream, in);
		queueForOutputBuffer.push(in);
		queueForFileBuffer.push(in);
	}
	finishedInputReading = true;
	pthread_exit(0);
}

void* writeToStream(void *args) {
	threadInfoStruct *t_info = (threadInfoStruct*)args;
	while(true) {
		if(finishedInputReading) {
			if(t_info->buffer.empty()) {
				break;
			}
		}
		if(!t_info->buffer.empty()) {
			t_info->stream << t_info->buffer.front() << endl;
			t_info->buffer.pop();
		}
	}
	pthread_exit(0);
}